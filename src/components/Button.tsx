import styled from "styled-components";
import {
  applyStyleModifiers,
  styleModifierPropTypes,
} from "styled-components-modifiers";
import {
  defaultTheme,
  typeScale,
  blue,
  neutral,
  yellow,
  red,
  green,
} from "../util";

const BUTTON_MODIFIER_CONFIG = {
  // The functions receive the props of the component as the only argument.
  // Here, we destructure the theme from the argument for use within the modifier styling.

  large: () => `
    font-size: ${typeScale.header4};
    padding: 24px 72px;
  `,
  warning: () => `
    background-color: ${yellow["100"]};
    &:hover {
      background-color: ${yellow["300"]}
    }
    &:active {
      outline-color: ${yellow["300"]}
    }
  `,
  error: () => `
    background-color: ${red["100"]};
    &:hover {
      background-color: ${red["300"]}
    }
    &:active {
      outline-color: ${red["300"]}
    }
  `,
  green: () => `
    background-color: ${green["100"]};
    &:hover {
      background-color: ${green["300"]}
    }
    &:active {
      outline-color: ${green["300"]}
    }
  `,
};

const Button = styled.button`
  padding: 12px 24px;
  font-size: ${typeScale.paragraphText};
  border-radius: 2px;
  min-width: 100px;
  cursor: pointer;
  font-family: "Quicksand", sans-serif;
  margin: 20px;
  &:disabled {
    color: white !important;
    background: ${neutral[400]} !important;
    border: none !important;
    outline: none !important;
    cursor: not-allowed;
  }
`;

Button.propTypes = {
  // @ts-ignore
  modifiers: styleModifierPropTypes(BUTTON_MODIFIER_CONFIG),
};

const PrimaryButton = styled(Button)`
  background-color: ${defaultTheme.primaryColor};
  border: none;
  color: white;
  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
  font-size: ${typeScale["header5"]};
  &:hover {
    color: white;
    background-color: ${blue[500]};
  }
  &:active {
    outline: 3px solid ${blue[500]};
    outline-offset: 2px;
    box-shadow: none;
  }
  transition: background-color ease-in-out 0.075s, outline linear 0.075s,
    box-shadow linear 0.075s;
  ${applyStyleModifiers(BUTTON_MODIFIER_CONFIG)};
`;

const SecondaryButton = styled(Button)`
  font-size: ${typeScale["header5"]};
  color: ${defaultTheme.primaryColor};
  border: 3px solid ${defaultTheme.primaryColor};
  box-sizing: border-box;
  box-shadow: 0px 2px 10px rgba(0, 0, 0, 0.34);
  &:hover {
    color: ${blue[500]};
    background-color: ${blue[100]};
  }
  &:active {
    box-shadow: none;
    border: 3px solid ${blue[500]};
  }
  transition: all linear 0.075s;
  ${applyStyleModifiers(BUTTON_MODIFIER_CONFIG)};
`;

const TertiaryButton = styled(Button)`
  font-size: ${typeScale["header5"]};
  color: ${defaultTheme.primaryColor};
  background-color: white;
  border: None;
  box-sizing: border-box;
  &:hover {
    color: ${blue[500]};
    background-color: ${blue[100]};
  }
  &:active {
    outline: 3px solid ${blue[500]};
    outline-offset: 2px;
    box-shadow: none;
  }
  transition: all linear 0.075s;
  ${applyStyleModifiers(BUTTON_MODIFIER_CONFIG)};
`;

export { PrimaryButton, SecondaryButton, TertiaryButton };
