import React from "react";
import ReactDOM from "react-dom";
import {
  PrimaryButton,
  SecondaryButton,
  TertiaryButton,
} from "./components/Button";
import { GlobalStyle } from "./util";

const App = () => (
  <div>
    <PrimaryButton>Primary Button</PrimaryButton>
    <SecondaryButton>Secondary Button</SecondaryButton>
    <TertiaryButton>Tertiary Button</TertiaryButton>
    <PrimaryButton modifiers={["large", "green"]}>Large Button</PrimaryButton>
    <PrimaryButton modifiers="warning">Warning</PrimaryButton>
    <PrimaryButton modifiers="error">Error</PrimaryButton>
    <GlobalStyle />
  </div>
);

ReactDOM.render(<App />, document.getElementById("root"));
