export const blue = {
  100: "#D7E9F7",
  200: "#99D3FF",
  300: "#2193EB",
  400: "#0670C2",
  500: "#1F3F57",
};

export const green = {
  100: "#529e66",
  200: "#367b48",
  300: "#276738",
};

export const yellow = {
  100: "#e1c542",
  200: "#cab23f",
  300: "#b49e35",
};

export const red = {
  100: "#EB5757",
  200: "#CF2B2C",
  300: "#95353a",
};

export const neutral = {
  100: "#ffffff",
  200: "#f4f5f7",
  300: "#e1e1e1",
  400: "#737581",
  500: "#4a4b53",
  600: "#000000",
};
