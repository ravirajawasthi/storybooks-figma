export const primaryFont = '"Quicksand", sans-serif';

export const typeScale = {
  header1: "3.7rem",
  header2: "2.1rem",
  header3: "1.6rem",
  header4: "1.2rem",
  header5: "1.1rem",
  paragraphText: "1rem",
  helperText: "0.7rem",
  copyrightText: "0.59rem",
};
